from django.urls import path
from .views import bookSearchView, add_like, top_books

urlpatterns = [
    path('story9/', bookSearchView, name='bookSearch'),
    path('addlike/', add_like, name='add_like'),
    path('topbooks/', top_books, name='top_books'),
]