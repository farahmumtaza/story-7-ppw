from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import bookSearchView
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from .models import LikedBook

# Create your tests here.
class Story9UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/story9/')
        self.page_content = self.response.content.decode('utf8')

    def test_story9_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story9_check_template_used(self):
        self.assertTemplateUsed(self.response, 'booksearch.html')
    
    def test_story9_check_function_used(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, bookSearchView)

    def test_story9_create_like_object(self):
        likedbook = LikedBook.objects.create(bookID = 'ABC', cover="abcdefg", title = 'ABC of Reading', author='Ezra Pound', publisher='Ezra Pund', publishedDate='24-01-00', likes=1)
        self.assertEqual(LikedBook.objects.count(), 1)
