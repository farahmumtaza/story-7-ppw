from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import LikedBook
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

# Create your views here.
def bookSearchView(request):
    return render(request, 'booksearch.html')

@csrf_exempt
def add_like(request):
    data = json.loads(request.body)

    try:
        likedBook = LikedBook.objects.get(bookID= data['id'], title=data['volumeInfo']['title'])
        likedBook.likes +=1
        likedBook.save()

    except LikedBook.DoesNotExist:
        item = data['volumeInfo']
        image = item['imageLinks']

        likedBook = LikedBook.objects.create(
            bookID = data['id'],
            cover = image.get('smallThumnail', '-'),
            title = item['title'],
            author = item.get('authors', '-'),
            publisher = item.get('publisher', '-'),
            publishedDate = item.get('publishedDate', '-'),
            likes = 1
        )
    return JsonResponse(likedBook.likes, safe=False) 

def top_books(request):
    lstTopBooks = []

    if LikedBook.objects.count() < 5 :
        j = LikedBook.objects.count()
    else :
        j = 5

    for book in LikedBook.objects.order_by('-likes')[0:j]:
        #remove brackets from book.author
        author = ''.join(c for c in book.author if c not in "[']")

        lstTopBooks.append({
            'cover':book.cover,
            'title': book.title,
            'author':author,
            'likes':book.likes
        })

    print(lstTopBooks)
    return JsonResponse(lstTopBooks, safe=False)

 