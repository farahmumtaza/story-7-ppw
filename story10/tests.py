from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
import time
from .views import logIn, signUp, logOut
from .apps import Story10Config
from selenium.webdriver.chrome.options import Options
import os

# Create your tests here.

class UnitTestonStory10(TestCase):

    #LOGIN PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logIn)

    def test_does_login_views_show_correct_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

    #SIGN UP PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)
    
    def test_signup_header(self):
        response = Client().get('/signup/')
        self.assertContains(response, "Create a new account")

    def test_does_signup_views_show_correct_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')
    
    #SIGN OUT TEST
    def test_does_sign_out_work(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

class FunctionalTestonStory10(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_signup_and_login(self):
        self.browser.get(self.live_server_url + '/signup/')
        time.sleep(2)

        firstname = self.browser.find_element_by_id('signUp-firstName')
        firstname.send_keys('test')

        lastname = self.browser.find_element_by_id('signUp-lastName')
        lastname.send_keys('test')

        username = self.browser.find_element_by_id('signUp-username')
        username.send_keys('test')

        password1 = self.browser.find_element_by_id('signUp-password1')
        password1.send_keys('storysepuluh10')

        password2 = self.browser.find_element_by_id('signUp-password2')
        password2.send_keys('storysepuluh10')

        signup_button = self.browser.find_element_by_id('btn-signUp')
        signup_button.click()

        time.sleep(3)
        self.assertIn("Welcome,", self.browser.page_source)
        self.assertIn("test", self.browser.page_source)

        logout_button = self.browser.find_element_by_id('btn-logOut')
        logout_button.click()
        time.sleep(3)

        userName = self.browser.find_element_by_id('signIn-username')
        userName.send_keys('test')

        passWord = self.browser.find_element_by_id('signIn-password')
        passWord.send_keys('storysepuluh10')

        login_button = self.browser.find_element_by_id('btn-logIn')
        login_button.click()

        time.sleep(3)
        self.assertIn("Welcome,", self.browser.page_source)
        self.assertIn("test", self.browser.page_source)

    def test_login_fail(self):
        self.browser.get(self.live_server_url + '/login/')
        time.sleep(2)

        userName = self.browser.find_element_by_id('signIn-username')
        userName.send_keys('halo')

        passWord = self.browser.find_element_by_id('signIn-password')
        passWord.send_keys('halohalobandung')

        login_button = self.browser.find_element_by_id('btn-logIn')
        login_button.click()

        time.sleep(3)
        self.assertIn("Invalid entry", self.browser.page_source)

        userName = self.browser.find_element_by_id('signIn-username')
        userName.send_keys('test')

        passWord = self.browser.find_element_by_id('signIn-password')
        passWord.send_keys('salah')

        login_button = self.browser.find_element_by_id('btn-logIn')
        login_button.click()

        time.sleep(3)
        self.assertIn("Invalid entry", self.browser.page_source)
    
    def test_signup_failed(self):
        self.browser.get(self.live_server_url + '/signup/')
        time.sleep(2)

        firstname = self.browser.find_element_by_id('signUp-firstName')
        firstname.send_keys('halo')

        lastname = self.browser.find_element_by_id('signUp-lastName')
        lastname.send_keys('halo')

        username = self.browser.find_element_by_id('signUp-username')
        username.send_keys('halo')

        password1 = self.browser.find_element_by_id('signUp-password1')
        password1.send_keys('test')

        password2 = self.browser.find_element_by_id('signUp-password2')
        password2.send_keys('test')

        signup_button = self.browser.find_element_by_id('btn-signUp')
        signup_button.click()

        time.sleep(3)
        self.assertIn("Invalid entry", self.browser.page_source)



class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story10Config.name, "story10")