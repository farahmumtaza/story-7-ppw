from django.test import TestCase, Client,  LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
import time
from .views import about
import os

class Story8UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/story8/')
        self.page_content = self.response.content.decode('utf8')

    def test_story8_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story8_check_template_used(self):
        self.assertTemplateUsed(self.response, 'about.html')
    
    def test_story8_check_function_used(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, about)
    
    def test_story8_content_exists(self):
        self.assertIn('Activities', self.page_content)
        self.assertIn('Experiences', self.page_content)
        self.assertIn('Achievements', self.page_content)


class Story8FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_website(self):
        self.browser.get(self.live_server_url + '/story8')
        # self.browser.get('http://localhost:8000/story8/')

        #Check if accordion exists
        self.assertIn("+ Activities", self.browser.page_source)
        self.assertIn("+ Experience", self.browser.page_source)
        self.assertIn("+ Achievements", self.browser.page_source)
        self.assertIn('<div class="main-about', self.browser.page_source)
        self.assertIn('<div class="about-container', self.browser.page_source)
        self.assertIn('<div class="accordion-container', self.browser.page_source)
        self.assertIn('<div class="accordion', self.browser.page_source)
        time.sleep(3)

        #Check if accordion1's content exists
        self.browser.find_element_by_class_name('accordion1').click()
        self.assertIn("Pejeje", self.browser.page_source)
        self.assertIn("Belanja stock rumah setiap 2 minggu", self.browser.page_source)
        time.sleep(3)

        #Check if accordion2's content exists
        self.browser.find_element_by_class_name('accordion2').click()
        time.sleep(3)
        self.assertIn("Sebenernya belum ada yang keren-keren banget", self.browser.page_source)
        self.assertIn("Tapi semoga quarantine cepat kelar ya", self.browser.page_source)
        time.sleep(3)

        #Check if accordion3's content exists
        self.browser.find_element_by_class_name('accordion3').click()
        time.sleep(3)
        self.assertIn("On", self.browser.page_source)
        self.assertIn("The", self.browser.page_source)
        self.assertIn("Way", self.browser.page_source)
        time.sleep(3)

        #Check theme switch
        self.browser.find_element_by_id('dot2').click()
        time.sleep(3)
        bg = self.browser.find_element_by_class_name('main-about').value_of_css_property('background-color')
        self.assertIn('rgba(225, 221, 238, 1)', bg)
        time.sleep(3)
        self.browser.find_element_by_id('dot1').click()
        time.sleep(3)
        bg = self.browser.find_element_by_class_name('main-about').value_of_css_property('background-image')
        self.assertIn('url("' + self.live_server_url + '/static/bg.png")', bg)



    
    def test_up_down_button(self):
        self.browser.get(self.live_server_url + '/story8')
        # self.browser.get('http://localhost:8000/story8/')

        #Get buttons by path
        upBtn = self.browser.find_element_by_xpath("//div[@class='accordion1 card z-depth-0 bordered'][contains(.,'Activities')]/div[1]/div[1]/div[1]")
        downBtn =  self.browser.find_element_by_xpath("//div[@class='accordion1 card z-depth-0 bordered'][contains(.,'Activities')]/div[1]/div[1]/div[2]")
        
        #Check default order
        #Activities should appear before Experiences
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') < page_source.find('Experiences'))

        #Check order when down button in "+ Activities" is clicked.
        #Activities should appear after Experiences
        downBtn.click()
        page_source = self.browser.page_source
        # print(page_source)
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') > page_source.find('Experiences'))

        #Check order when up button in "+ Activities" is clicked
        #Activities should appear before Experiences
        upBtn.click()
        page_source = self.browser.page_source
        # print(page_source)
        print("index of activities is: " + str(page_source.find('Activities')))
        print("index of experiences is: " + str(page_source.find('Experiences')))
        self.assertTrue(page_source.find('Activities') < page_source.find('Experiences'))
