from django.urls import path
from .views import about

urlpatterns = [
    path('story8/', about, name='about'),
]