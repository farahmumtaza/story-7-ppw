from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=20, label="Name", widget=forms.TextInput(attrs={
        'class':'form-control input-form my-2', 
        'placeholder' : 'Enter your name', 
        'style': 'height:8vh', 
        'required': 'True', 
        }))
    message = forms.CharField(max_length=255, label="Status", widget=forms.TextInput(attrs={
        'class':'form-control input-form my-2', 
        'placeholder' : 'What\'s up?', 
        'style': 'height: 20vh',
        'required': 'True', 
        }))
