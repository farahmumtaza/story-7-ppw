from django.db import models

# Create your models here.
class Status(models.Model) :
    name = models.CharField(max_length=20, blank=False)
    message = models.CharField(max_length=255, blank=False)
    date = models.DateTimeField(auto_now_add=True, null=True)
