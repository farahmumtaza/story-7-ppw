# PPW POST-UTS STORY
This repository contains work from Story 7 & Story 8.

Deployed at https://farah-story7.herokuapp.com

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/farahmumtaza/story-7-ppw/badges/master/pipeline.svg)](https://gitlab.com/farahmumtaza/story-7-ppw/commits/master)
[![coverage report](https://gitlab.com/farahmumtaza/story-7-ppw/badges/master/coverage.svg)](https://gitlab.com/farahmumtaza/story-7-ppw/commits/master)