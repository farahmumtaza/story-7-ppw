var likedBooks =[];

$(document).ready(function() {
    $("#search-input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q); //q is the search query

        //check if sidebar is in frame then toggle sidebar out
        if ( !$('#sidebar2').hasClass('active')){
            $('#sidebarCollapse').click();
        }

        var xhttpr = new XMLHttpRequest();
        xhttpr.onreadystatechange = function() {
            if(this.readyState == 4){
                if(this.status == 200){
                    var data = JSON.parse(this.responseText);
                    console.log('data', data); //check processed data
                    likesBooks= data.items;

                    $('#search-result').html('') //reset page content every time after search is triggered
                    var result = '';
                    for (var i = 0; i < data.items.length; i++) {
                        item = data.items[i].volumeInfo;

                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" +
                            item.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                            "<td class='align-middle'>" + item.title + "</td>" +
                            "<td class='align-middle'>" + item.authors + "</td>" +
                            "<td class='align-middle'>" + item.publisher + "</td>" +
                            "<td class='align-middle'>" + item.publishedDate + "</td>" +
                            "<td class='align-middle'>" +
                            "<button type ='button' class='like-btn btn btn-link' onclick='liked("+i+")'><img id ='likeIcon"+ i +"' src='https://img.icons8.com/ios/24/000000/like.png'/></button>" +
                            "<p id ='like"+ i +"' value='0'>0</p>"+
                            "</td>"+"</tr>"
                        }
                    $('#search-result').append(result);

                } else {
                    alert("Sorry, we don't have the book you're looking for:(")
                }
            } 
        }
        xhttpr.open("GET","https://www.googleapis.com/books/v1/volumes?q=" + q,true);
        xhttpr.send();
       
    });
});

//Like feature triggered onclick
function liked(i){
    var data = JSON.stringify(likesBooks[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4){
            if (this.status ==200){
                var response = JSON.parse(this.responseText)
                console.log('response:', response); //check number of likes
                $("#like"+i).html(response);
                $("#likeIcon"+i).attr('src', 'https://img.icons8.com/ios-filled/24/000000/like.png');

            } else {
                alert("Cannot like the book rn. Pls try again later.")
            }
        }
    }
    xhttpr.open("POST","/addlike/");
    xhttpr.send(data);  
}

//Get top 5 books
function getTopBooks(){
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange = function(){
        if(this.readyState == 4){
            if(this.status == 200){
                console.log(JSON.parse(this.responseText));
                var response = JSON.parse(this.responseText);
                console.log('toplist:', jQuery.type(response))

                $('.modal-body').html(''); //reset modal body
                console.log($('#modal-body').html())
                var result = "";

                if (response.length < 5){
                    var j = response.length;
                } else {
                    var j = 5;
                }

                console.log(j)
                var i=0;
                while (i<j) {
                    console.log('topbooks', response[i]);

                    const title = response[i]['title'];
                    const author = response[i]['author'];
                    const likes = response[i]['likes'];

                    result += "<div class='card my-3 mx-auto' style='width: 18rem;'>" +
                                "<div class='card-body'>" +
                                "<h5 class='card-title'>" + title + "</h5>" +
                                "<h6 class='card-subtitle mb-2 text-muted'> by " + author + "</h6>" +
                                "<p class='card-text'> <img class='mx-2' src='https://img.icons8.com/ios-filled/24/000000/like.png'/>" + likes + "</p>" +
                                "</div>" +
                                "</div>"

                    $('.modal-body').html(result)
                    i++;
                }
            } else {
                alert("Top books cannot be loaded.");
            }
        }
    }
    xhttpr.open("GET","/topbooks/",true)
    xhttpr.send();
}
